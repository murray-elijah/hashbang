local groups = {
	developer = {
		"136523378822610945",
		"107827535479353344"
	}
}

function groups:getUserGroups()
	return groups.list
end

function groups:getIsInGroup(user, group)
	for _,v in pairs(group) do
		if user == v then
			return true
		end
	end
	return false
end

return groups
