local hashbang = _G.hashbang

local command = {}

command.text = "Checks that the bot's responding."
command.syntax = ""
command.keyword = "ping"

command.event = "messageCreate"

function command.func(message)
	if not message.author.bot then
		if message.content == "#!ping" then
			message:reply("Pong!")
		end
	end
end

return command
