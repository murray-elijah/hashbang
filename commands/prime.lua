--%18.0f

local hashbang = _G.hashbang
local command = {}

command.text = "Checks if a number is prime. If not, returns the prime factors. Note: limited to 30 characters for digits, because luna crashed it with a very large number. Luna, if you're reading this, damn you but also thanks for the stress testing :heart:"
command.syntax = "<int>"
command.keyword = "prime"

command.event = "messageCreate"

function command.func(message)
	if not message.author.bot then
		if hashbang.util.starts(message.content, "#!prime") then
			local n = tonumber(string.sub(message.content, 8))
			if n == nil or type(n) ~= "number" then return message:reply("An actual number, please. In base ten.") end 
			if n > 2^25 then return message:reply("Too big for my processor :c") end
			if n < 1 then return message:reply(n .. " is not a valid input.") end
			if math.floor(n) ~= n then return message:reply(n .. " is not a whole number.") end
			if n == 1 then
				return message:reply(n.." is not prime!")
			elseif n == 2 then
				return message:reply(n.." is prime!")
			else
		for i = 3, n-1 do
			if n % i == 0 then
				if n == 80085 then return message:reply(primeFactors(n) .. "\nAlso, boobs. Heh.") end
				return message:reply(primeFactors(n))
			end
		end
				return message:reply(n.." is prime!")
			end
		end
	end
end

function primeFactors(n)
	local n2 = n
	local factors = {}

	local count = {}
	
	count[2] = 0
	while n % 2 == 0 do
		count[2] = count[2] + 1
		n = n / 2
	end
	
	if count[2] == 1 then
		table.insert(factors, "2")
	elseif count[2] > 1 then
		table.insert(factors, "2^"..count[2])
	end
	
	for i = 3, n, 2 do
		count[i] = 0
		while n % i == 0 do
			count[i] = count[i] + 1
			n = n / i
		end
		if count[i] == 1 then
			table.insert(factors, i)
		elseif count[i] > 1 then
			table.insert(factors, i.."^"..count[i])
		end
	end

	if n > 2 then
		table.insert(factors, n)
	end
	
	local response = {}
	table.insert(response, n2 .. " is not prime!")
	table.insert(response, "```\n" .. table.concat(factors, " * ") .. "```")
	
	local ap = true

	for k,v in ipairs(factors) do
		if count[k] ~= 1 then
			ap = false
		end
	end

	if ap and #factors == 2 then
		table.insert(response, n2 .. " is an Alice number! (2^n + prime)")
	end
	if isSemiPrime(count) then
		table.insert(response, n2 .. " is a semiprime!")
	end

	if n == 80085 then
		table.insert(response, "Hah, boobs.")
	end

	return table.concat(response, "\n")
	
end

function isSemiPrime(table)
	local count = 0

	for k,v in pairs(table) do
		if v == 1 then count = count + 1 end
	end

	return (count == 2)
end

return command
