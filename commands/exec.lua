local hashbang = _G.hashbang

local starts = hashbang.util.starts

local command = {}

command.text = "Executes code."
command.syntax = "<code in codeblock w/ syntax highlight>"
command.keyword = "/bin/lua"

command.event = "messageCreate"
command.ignore = true

Colour = hashbang.api.Color

function printLine(...)
	local ret = {}
	for i = 1, select('#', ...) do
		local q = tostring(select(i, ...))
		table.insert(ret, q)
	end
	return table.concat(ret, '\t')
end

function getSandbox(m)
	local sb = {}
	
	sb.inspect = hashbang.libraries.inspect
	sb.me = m.author
	sb.Object = require('core').Object
	
	sb.message = m

	if isDeveloper(m.author.id) then
		setmetatable(sb, {__index = _G})
		sb.printConsole = print
	elseif m.channel.id == "449086785239253002" then
		sb.coroutine = coroutine
		sb.tostring = tostring
		sb.tonumber = tonumber
		sb.ipairs = ipairs
		sb.pairs = pairs
		sb.string = string
		sb.table = table
		sb.math = math
		sb.type = type
		sb.debug = debug
		sb.error = error
		sb.hook = hook
		sb.json = require("json")
	end
	
	return sb
end

function command.func(message)
	if not message.author.bot then
		if starts(message.content, "#!/bin/lua") and ( isDeveloper(message.author.id) or message.channel.id == "449086785239253002" ) then
			
			local s = string.sub(message.content, 12)

			if starts(s, "```lua") then s = string.sub(s, 8, -4) end
			if starts(s, "```") then s = string.sub(s, 5, -4) end
			
			s = s:gsub("```", "")

			local l = {}
			local sandbox = getSandbox(message)
			
			sandbox.print = function(...)
				table.insert(l, printLine(...))
			end
						
			local fn, syntaxError = load(s, 'HashBang', 't', sandbox)
			if not fn then return message:reply{embed = { title = "Syntax Error!", author = {name = message.author.username, icon_url = message.author.avatarURL}, fields = {{name = "Input:", value = code(s, true), inline = false}, {name = "Error:", value = code(syntaxError, false), inline = false}}, color = 0xFF0000}} end
	
			
			local success, runtimeError = pcall(fn)
			if not success then return message:reply{embed = { title = "Runtime Error!!", author = {name = message.author.username, icon_url = message.author.avatarURL}, fields = {{name = "Input:", value = code(s, true), inline = false}, {name = "Error:", value = code(runtimeError, false), inline = false}}, color = 0xFFFF00}} end
			
			l = table.concat(l, '\n')
			return message:reply{embed = { title = "Success!!", author = {name = message.author.username, icon_url = message.author.avatarURL}, fields = {{name = "Input:", value = code(s, true), inline = false}, {name = "Result:", value = code(l, false), inline = false}}, color = 0x00FF00}}
		end
	end
end

function code(x, h)
	if h then return "```lua\n"..x.."```" end
	return "```\n"..x.."```"
end

function isDeveloper(id)
	return 
		id == "136523378822610945" or -- Eli
		id == "107827535479353344" or -- homonovus
		id == "131869343611879426" or -- Tribbeanie
		id == "171712009925165056" -- Tim
end

return command
