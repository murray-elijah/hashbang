local hashbang = _G.hashbang

local command = {}

command.text = "Checks that the bot's responding."
command.syntax = "<channel ID or END>"
command.keyword = "ctrl"

command.event = "messageCreate"
command.ignore = true

function command.func(message)
	if message.channel.id == "574846520562548736" and starts(message.content, "#!ctrl") then
		local state = message.content:sub(8)
		if message.author ~= hashbang.client.user then
			if state == "END" then
				_G.hashbang.states.listening = false
				message:reply("Control released.")
			else
				_G.hashbang.states.listening = state
				message:reply("Control enabled in "..state)
			end
		end
	end
	
	if message.channel.id == "574846520562548736" and message.author ~= hashbang.client.user and _G.hashbang.states.listening ~= false and not starts(message.content, "#!ctrl") then
		hashbang.client:getChannel(_G.hashbang.states.listening):send(message.content)
	end
	
	if _G.hashbang.states.listening ~= false and message.channel.id == _G.hashbang.states.listening and message.author ~= hashbang.client.user then
		return hashbang.client:getChannel("574846520562548736"):send("**"..message.author.name..": **"..message.content)
	end
end

function starts(str, start)
	return str:sub(1,#start) == start
end

return command
