local hashbang = _G.hashbang

local command = {}

command.text = "Displays this menu."
command.syntax = ""
command.keyword = "help"

command.event = "messageCreate"

function command.func(message)
	if not message.author.bot then
		if message.content == "#!help" then
			
			local cmds = {}
			for _,v in pairs(hashbang.commands) do
				if not v.ignore then
					local p = " "
					if v.syntax == "" then p = "" end
					local x = "**"..hashbang.config.prefix..v.keyword..p..v.syntax.."**: "..v.text
					table.insert(cmds, x)
				end
			end
			message:reply("*#!* bot by Eli\n\n"
				.. "**Commands:**\n" .. table.concat(cmds,"\n"))
		end
	end
end

return command
