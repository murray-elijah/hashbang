local hashbang = _G.hashbang

local command = {}

command.text = "Convert numbers between bases."
command.syntax = "<number> <from> <to>"
command.keyword = "base"

command.event = "messageCreate"

local ins = require('inspect')

function command.func(message)
	if not message.author.bot then
		if starts(message.content, "#!base") then
			local cmd = split(message.content)
			for word in message.content:gmatch("%w+") do table.insert(cmd, words) end
			if table.getn(cmd) == 4 then
				cmd[3] = tonumber(cmd[3])
				cmd[4] = tonumber(cmd[4])
				message:reply(string.format("%18.0f", basen(tonumber(cmd[2], cmd[3]), cmd[4])))
			else
				message:reply("Error! #!base <number> <from> <to>")
				print(ins(cmd))
			end
		end
	end
end

function starts(str, start)
	return str:sub(1,#start) == start
end

function split(s, d)
	if not d then d = " " end
	result = {}
	for match in (s..d):gmatch("(.-)"..d) do
		table.insert(result, match)
	end
	return result
end

function basen(n,b)
    n = math.floor(n)
    if not b or b == 10 then return tostring(n) end
    local digits = "0123456789abcdefghijklmnopqrstuvwxyz"
    local t = {}
    local sign = ""
    if n < 0 then
        sign = "-"
    n = -n
    end
    repeat
        local d = (n % b) + 1
        n = math.floor(n / b)
        table.insert(t, 1, digits:sub(d,d))
    until n == 0
    return sign .. table.concat(t,"")
end

return command
