local hashbang = _G.hashbang

local command = {}

command.text = "List bot guilds."
command.syntax = ""
command.keyword = "guilds"

command.event = "messageCreate"
command.ignore = true

function command.func(message)
	if message.author == hashbang.client.owner then
		if message.content == "#!guilds" then
			local response = {}
			for k,v in pairs(hashbang.client.guilds) do
				table.insert(response, k .. ": " .. hashbang.client:getGuild(k).name)
			end
			message:reply("```\n" .. table.concat(response, "\n") .. "```")
		end
	end

	if not message.author.bot and message.author ~= hashbang.client.owner and message.content == "#!guilds" then
		message:reply("no")
	end
end

return command
