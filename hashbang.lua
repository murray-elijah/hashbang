local discordia = require('discordia')

local fs = require('fs')

local inspect = require('inspect')

_G.hashbang = {
	api = discordia,
	client = discordia.Client(),
	config = require('./config.lua'),
	commands = {},
	libraries = {
		groups = require('./libraries/groups.lua'),
		inspect = require('inspect'),
		Object = require('core').Object,
		json = require('json'),
		request = require('request')
	},
	states = {
		listening = false
	},
	util = {}
}

local hashbang = _G.hashbang

hashbang.client:on('ready', function()
	print("Logged in!\n")
	
	hashbang.states.listening = false

	loadFromFolder('commands')
	for k,v in pairs(hashbang.client.guilds) do
		hashbang.client:getGuild(k).me:setNickname(hashbang.config.autoNick)
		hashbang.util.wait(1)
	end

--	print(inspect(hashbang.commands))
end)

hashbang.client:run("Bot " .. hashbang.config.token)

function loadFromFolder(path)
	for _,f in pairs(fs.readdirSync("./"..path)) do
		registerCommand("./"..path.."/"..f)
	end
	initCommands()
end

function _G.hashbang.util.split(str, sep)
	local t = {}
	for word in str:gmatch("%w+") do
		table.insert(t, word)
	end
	return t
end

function _G.hashbang.util.starts(str, start)
	return str:sub(1,#start) == start
end

function _G.hashbang.util.wait(n)
	local t0 = os.clock()
	while os.clock() - t0 <= n do end
end

function registerCommand(command)
	if not require(command).indev then
		table.insert(hashbang.commands, require(command))
	end
end

function initCommands()
	for _,v in pairs(hashbang.commands) do
		hashbang.client:on(v.event, v.func)
		print("[MODULE] Loaded " .. v.keyword)
	end
end
